const oracledb = require('oracledb');
const express = require('express');


const app = express();
const bodyParser = require('body-parser');
const PORT = process.env.PORT || 5000;

app.use(bodyParser.json()); 

app.use(bodyParser.urlencoded({ extended: true}));


oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;
oracledb.autoCommit = true;


app.get('/show',async(req,res)=>{
    let connection = null
    try{
        let connection = await oracledb.getConnection({
            user: "kv11",
            password: "kv11",
            connectString: "192.168.1.94:1521/srorcl"
        });
        const result = await connection.execute(
                        `SELECT * FROM persons`
                    );
                    console.log(result.rows)
                    res.send(result.rows)
    }
    catch(err){
        console.log(err)
        return res.send("Error")
    }
    finally{
        if(connection){
            try{
                await connection.close()
            }
            catch(err){
                console.log("Finally error")
                console.log(err)
            }
        }
    }
})



app.post('/send', async (req, res) => {
    console.log(req.body)
    let connection = null
    try {
        let connection = await oracledb.getConnection({
            user: "kv11",
            password: "kv11",
            connectString: "192.168.1.94:1521/srorcl"
        });
        const insert_q = `INSERT INTO persons (id,lastname,firstname,age,email,address,password) values (:Id, :lastname, :firstname, :age,:email,:address, :password)`;
        var Id = req.body.Id;
        var lastname = req.body.lastname;
        var firstname = req.body.firstname;
        var age = req.body.age;
        var email  = req.body.email;
        var address = req.body.address;
        var password = req.body.password;
        let bind = []
        
        bind.push(id, lastname,firstname,age,email,address,password);
      
        var binds = [];

        binds.push(bind);

        insert = await connection.executeMany(insert_q, binds)
        //console.log(binds)
        return res.send(binds)
    }
    catch (err) {
        console.log(err)
        return res.send("Error")
    }
    finally{
        if(connection){
            try{
                await connection.close()
            }
            catch(err){
                console.log("finally error")
                console.log(err)
            }
        }
    }  
});




app.post('/update',async (req,res)=>{
    let connection = null
    try{
        connection = await oracledb.getConnection({
            user: "kv11",
            password: "kv11",
            connectString: "192.168.1.94:1521/srorcl"
        });

        const update = `UPDATE persons SET id = :id WHERE lastname = :lastname`;
        const result = await connection.execute(update,{lastname: req.body.lastname, id: req.body.id})
        console.log(JSON.stringify(result))
        res.json(result)
    }
    
    catch(err){
    
        console.log(err)
    
    }
    
    finally{
        if(connection){
            try{
                await connection.close()
            }
            catch(err){
                console.log("Finally error")
                console.log(err)
            }
        }
    }

})




app.post('/remove',async(req,res)=>{
    let connection = null
    try{
        connection = await oracledb.getConnection({
            user: "kv11",
            password: "kv11",
            connectString: "192.168.1.94:1521/srorcl"
        });
  
        const del = `DELETE FROM persons WHERE firstname = :firstname`;
        const result = await connection.execute(del, {firstname: req.body.firstname});
        console.log(JSON.stringify(result));
        res.json(result)
  
    }
    catch(err){
        console.log(err)
    }
    finally{
        if(connection){
            try{
                await connection.close()
            }
            catch(err){
                console.log("Finally error");
                console.log(err);
            }
        }
    }
  })

app.listen(PORT, () => console.log(`server started on port ${PORT}`));
